# Summary Statistics Association Counter

This script was designed to generate a plot for a NAR paper of the GWAS Catalog. The task was to demonstrate how the size of the stored summary statistics files was growing in the last year. The script reads study IDs with summary stats files, looks up the ftp area, and counts the lines in the stored files. At the end a detailed report is generated for each counted lines.

More information at the Confluence page of the task: (LINK)[https://www.ebi.ac.uk/seqdb/confluence/display/GOCI/Counting+associations+in+the+deposited+summary+statistics+files]

## Generation of the input file

```sql
SELECT
 S.ACCESSION_ID,
 S.SNP_COUNT,
 TO_CHAR(HK.CATALOG_PUBLISH_DATE, 'YYYY-MM-DD') as CATALOG_PUBLISH_DATE,
 P.PUBMED_ID,
 A.FULLNAME_STANDARD,
 TO_CHAR(P.PUBLICATION_DATE, 'YYYY-MM-DD') as PUBLICATION_DATE,
 TO_CHAR(HK.CATALOG_UNPUBLISH_DATE, 'YYYY-MM-DD') as CATALOG_UNPUBLISH_DATE
FROM STUDY S, HOUSEKEEPING HK, PUBLICATION P, AUTHOR A
WHERE S.FULL_PVALUE_SET = 1 and HK.ID = s.HOUSEKEEPING_ID AND P.ID = S.PUBLICATION_ID AND HK.IS_PUBLISHED = 1 and
        P.FIRST_AUTHOR_ID = A.ID;
```

### Run the script on the farm


```bash
today=$(date "+%Y.%m.%d")
inputTable="Studies_with_summaryStats_2018.07.24.tsv"
studyCnt=$(tail -n+2 $inputTable | wc -l )
bsub -M3000 -J rowCount[2-$studyCnt] -R"select[mem>3000] rusage[mem=3000]" -o logs/rowCount.${today}.%I.o -e logs/rowCount.${today}.%I.e "./count_row_numbers.sh ${inputTable}"
```
