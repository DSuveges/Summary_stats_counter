#!/usr/bin/env bash

### 
### List of functions
###

# Get the extension of a given file:
function get_extensinon() {
    testFile="${1}"
    ext=$(echo "$testFile" |  perl -lane 'if($_ =~ /\.tar/ ){ ($ext) = $_ =~ /(tar.{0,5})$/;}else{($ext) =$_ =~ /\.(.{1,4})$/}; print lc $ext')
    echo ${ext:-NA}
}

# Get the md5 sum of the file:
function get_md5sum(){
    testFile="${1}"
    md5=$(md5sum "${testFile}" | cut -f1 -d" ")
    echo ${md5:-NA}
}

# Get deposition data of the data: 
function get_depostion_date(){
    testDir="${1}"
    depositoinDate=$(find "${testDir}"  -type f -printf '%TY/%Tm/%Td\n' | sort | head -n 1)
    echo ${depositoinDate:-NA}
}

# Get row number from uncompressed files:
function get_rowNum_raw(){
    testFile="${1}"
    rowNum=$(cat "${testFile}" | dos2unix | dos2unix -c mac | wc -l )
    echo ${rowNum:-NA}
}

# Get row number for gzipped files:
function get_rowNum_gzip(){
    testFile="${1}"
    rowNum=$(zcat "${testFile}" | wc -l )
    echo ${rowNum:-NA}
}

# Get row number for gzipped files:
function get_rowNum_tar(){
    testFile="${1}"
    accesptedExtensions="csv tsv gz bgz txt assoc out"

    fileList=()
    while read f; do
        fExt=$(get_extensinon "${f}")
        if [[ "${accesptedExtensions}" == *$fExt* ]]; then
            fileList+=("${f}")
        else
            echo -e "\t[Warning] $f will be excluded from $testFile. Will not be counted. Not countable." >> ${reportFile};
        fi
    done < <(tar -tf "${testFile}" | grep -v -F -i ${exclusionPattern} -e md5 | awk '$1 !~ /\/$/ && $1 !~ /\/\._/' )

    # Looping through the files and count:
    rowNum=0
    for f2read in "${fileList[@]}"; do
        
        if [[ $fExt == "gz" || $fExt == 'bgz' ]]; then
            singleCount=$(tar -xOzf "${testFile}" "$f2read" | gunzip | wc -l)
        else
            singleCount=$(tar -xOzf "${testFile}" "$f2read" | wc -l)
        fi
        echo -e "\t[Info] Counting $f2read - $singleCount" >> ${reportFile};
        rowNum=$(( $rowNum + $singleCount ))
    done

    echo ${rowNum:-NA}
}

function get_rowNum_zip(){
    accesptedExtensions="csv tsv gz bgz txt assoc out tar.gz"
    testFile="${1}"

    # Get a list of files within the archive that we will count:
    fileList=()
    while read f; do
        fExt=$(get_extensinon "${f}")
        if [[ "${accesptedExtensions}" = *$fExt* ]]; then
            echo $fExt >&2
            fileList+=("${f}")
        else
            echo "[Warning] $f will be excluded from $testFile. Not countable." >> ${reportFile};
        fi
    done < <(unzip -l "${testFile}" | sed -e 's/^\s*//g;s/  */ /g' | cut -f4- -d" " | awk '$1 != "Name" && $1 != "----" && $0 !~ /\/$/ && $1 != ""' | grep -v -F -i ${exclusionPattern} -e md5 )

    # Looping through the files and count:
    rowNum=0
    for f2read in "${fileList[@]}"; do
        fExt=$(get_extensinon "${f2read}")
        echo "[Info] Counting $f2read" >> ${reportFile};
        if [[ $fExt == "gz" || $fExt == 'bgz' || $fExt == 'tar.gz' ]]; then
            singleCount=$(unzip -qc "${testFile}" "$f2read" | gunzip | wc -l)
        else
            singleCount=$(unzip -qc "${testFile}" "$f2read" | wc -l)
        fi
        rowNum=$(( $rowNum + $singleCount ))
    done

    echo ${rowNum:-NA}
}

###
### script body
###

# Generate run date:
today=$(date "+%Y.%m.%d")

# Firts, let's define some variables:
scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # From where the script actually lives
sourceRootDir=/nfs/spot/data/prod/gwas/summary_statistics
tempRootDir=/nfs/spot/data/prod/gwas/preanalysis_summarystats
export exclusionPattern=' -e readme -e .DS_Store -e .tbi -e md5sum -e TRAIT_MAP -e license -e lookup -e info -e notes -e citation -e .doc -e "._" -e .pdf '
reportFolder="${scriptDir}/reports"
countFolder="${scriptDir}/counts"

# Input table:
if [[ -z "${1}" ]]; then
    echo "[Error] Input table is required. Exiting."
    exit 
elif [[ ! -e "${1}" ]]; then
    echo "[Error] Table file could not be read. Exiting."
    exit
else
    inputTable="${1}"
fi

# The line number it will read from the table is read from the command line:
if [[ ! -z "${2}" ]]; then
    lineNo="${2}"
elif [[ ! -z "${LSB_JOBINDEX}" ]]; then
    lineNo=${LSB_JOBINDEX}
else
    echo "[Error] Line number could not be parsed. Exiting."
    exit
fi

# Extracting fields from the file from the given row:
read ACCESSION_ID SNP_COUNT CATALOG_PUBLISH_DATE PUBMED_ID FULLNAME_STANDARD PUBLICATION_DATE CATALOG_UNPUBLISH_DATE <<< $( head -n $lineNo "${inputTable}" | tail -n1 | tr " " "_" )

# Generate folder name:
sourceDir="${sourceRootDir}/${FULLNAME_STANDARD//_/}_${PUBMED_ID}_${ACCESSION_ID}"

# Generate output file name:
reportFile="${reportFolder}/${FULLNAME_STANDARD//_/}_${PUBMED_ID}_${ACCESSION_ID}_report.txt"
countFile="${countFolder}/${FULLNAME_STANDARD//_/}_${PUBMED_ID}_${ACCESSION_ID}_counts.txt"

# Printing out reports:
echo -e "[Info] Run date: $today
[Info] Input table: $inputTable
[Info] Line from imput table: $lineNo
[Info] Extracted fields: $ACCESSION_ID, $PUBMED_ID, $FULLNAME_STANDARD, $PUBLICATION_DATE
[Info] Source folder: ${sourceDir}
" > "${reportFile}"

# Report problem with folders:
if [[ ! -d "${sourceDir}" ]]; then
    echo "[Error] $sourceDir does not exists" > "${reportFile}"
    exit
fi

# Filter files, and print out to report:
declare -a excludedFiles=()
declare -a sourceFiles=()
while read file; do excludedFiles+=("${file}");done < <(find "${sourceDir}" -type f | grep -F -i ${exclusionPattern})
while read file; do sourceFiles+=("${file}");done < <(find "${sourceDir}" -type f | grep -F -v -i ${exclusionPattern})

# Save a list of excluded files for further record:
if [[ ! -z "${excludedFiles}" ]]; then 
    echo -e "[Info] The following files will be excluded:" >> "${reportFile}"; 
    printf '\t%s\n' "${excludedFiles[@]}" >> ${reportFile}; 
fi

# Exit if there are no files to actually count:
if [[ -z "${sourceFiles}" ]]; then 
    echo "[Error] There is no file to count. Exiting." >> ${reportFile}; 
    exit; 
fi

echo -e "[Info] The following files will be counted:" >> "${reportFile}"; 
printf '\t%s\n' "${sourceFiles[@]}" >> ${reportFile}

# Get deposition data before lookgin at the files themselves:
depositoinDate=$( get_depostion_date "${sourceDir}" )
echo "[Info] Deposition date for this study: $depositoinDate" >> "${reportFile}"

# Check if count file exists or not:
if [[ ! -e ${countFile} ]]; then 
    # Creating the file with the header:
    echo -e "ACCESSION_ID\tPUBMED_ID\tFULLNAME_STANDARD\tDEPOSITION_DATE\tFOLDER\tFILE_NAME\tMD5_SUM\tROW_NUM" > "${countFile}"
fi

# Looping through all the files we have to count, and then check the extension:
for sourceFile in "${sourceFiles[@]}"; do
    FILE_NAME=$( basename "${sourceFile}" )

    # Get last modification date of the file:
    depositoinDate=$( stat --printf="%z\n" "${sourceFile}" | cut -f1 -d" " )

    extension=$(get_extensinon "${sourceFile}" )
    md5=$(get_md5sum "${sourceFile}" )

    # Check if a file has to be counted or not:
    read test_md5sum test_row_num <<< $(grep -w "${FILE_NAME}" "${countFile}" | cut -f7,8 )
    if [[ ! -z $test_md5sum && $test_md5sum == $md5 && $test_row_num != "NA" ]]; then
        echo "[Info] $sourceFile has already been counted. Skipping" >> "${reportFile}"
        continue
    elif [[ ! -z $test_md5sum &&  $test_md5sum == $md5 && ( $test_row_num == "NA" || $test_row_num -eq 0 ) ]]; then
        echo "[Warning] $sourceFile has already been counted, but the count is NA. Counting again." >> "${reportFile}"
    elif [[ ! -z $test_md5sum && $test_md5sum != $md5 ]]; then
        echo "[Warning] $sourceFile was counted but md5sum was different. Counting again." >> "${reportFile}"
    fi

    # Count lines based on extension:
    if [[ ! -z $(file "${sourceFile}" | grep ASCII ) ]]; then
        echo "[Info] ${sourceFile} is an ASCII file. Counting lines." >> "${reportFile}";
        rowNum=$( get_rowNum_raw "${sourceFile}" )
    elif [[ $extension == "zip" ]]; then
        echo "[Info] ${sourceFile} is zip compressed file. Counting lines." >> "${reportFile}";
        rowNum=$( get_rowNum_zip "${sourceFile}" )
    elif [[ $extension == "tar" || $extension == "tar.gz" || $extension == "tgz" ]]; then
        echo "[Info] ${sourceFile} is a tar file. Counting lines." >> "${reportFile}";
        rowNum=$( get_rowNum_tar "${sourceFile}" )
    elif [[ $extension == "xlsx" ]]; then
        echo "[Warning] ${sourceFile} is an excel file. Skipping." >> "${reportFile}";
        continue
    elif [[ $extension == "gz" || $extension == "bgz" ]]; then
        echo "[Info] ${sourceFile} is a gzipped file. Counting lines." >> "${reportFile}";
        rowNum=$( get_rowNum_gzip "${sourceFile}" )
    else 
        echo "[Warning] ${sourceFile} is an unknown binary file. Skipping." >> "${reportFile}";
        continue
    fi

    # Report warning if the number of lines in the files are too low:
    if [[ $rowNum -lt 5000 ]]; then
        echo "[Warning] ${sourceFile} contains too few lines: $rowNum."  >> "${reportFile}";
    fi

    # Save results if the count is not NA:
    if [[ ! -z $test_md5sum ]]; then 
        echo "[Info] Removing $sourceFile from count file." >> "${reportFile}"
        grep -w -v $ACCESSION_ID "${countFile}" | sponge "${countFile}"
    fi
    echo -e "${ACCESSION_ID}\t${PUBMED_ID}\t${FULLNAME_STANDARD}\t${depositoinDate}\t${sourceDir}\t${FILE_NAME}\t${md5}\t${rowNum}" >> "${countFile}"
done

